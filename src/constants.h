#ifndef CONSTANTS_H
#define CONSTANTS_H

#define EP_COOLDOWN 3
#define UNI_COOLDOWN 3              //Multiplied by distance travelled and difficulty
#define PEGA_COOLDOWN 10            //Multiplied by difficulty, but not by travel distance
                                    //Forgive me the inconsistences, they're miserable

enum directionVar{STAY, NORTH, SOUTH, WEST, EAST};    //Not sure where to fit that, so it's here I guess

#endif

//This is supposed to be basic definition macros, but I guess they should be moved to configuration file instead. TODO I guess.
