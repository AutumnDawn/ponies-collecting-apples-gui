// TODO clean things up more
/*
	Copyright (C) 2020  Autumn Dawn

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "include.h"
#include "constants.h"
#include "player.h"
#include "creatures.h"
#include "gui_render.h"
#include "terrain.h"
#include "block.h"

class Game
{
private:
	// Mostly internal
	unsigned short difficulty_multiplier;
	std::string difficulty_name;
	char input;
	bool restart;
	bool run;
	bool debug;
	class Render Render;
	SDL_Event event;
	
	// Parts of the world
	class Terrain Terrain;
	std::vector<class Creature*> Creature;
	enum PlayerRace PlayerChoosenRace;
	class Player *Player;
	int level;
	int score_target;
	
	// Statistics-related
	unsigned turn;
	int time_start;
	HUD hud;
	
	int init();
	void prepareGame();
	int runGame();
	void finalize();
	void cleanup();
public:
	int start()
	{
		int exit_code = 1;
		if (init() == 0)
			return 0;
		do {
			std::cout << "Preparing game.\n";
			prepareGame();
			Render.prepareGUI();
			Render.rerenderHud();
			run = true;
			std::cout << "Entering main loop.\n";
			do {
				exit_code = runGame();
			} while (run);
			restart = false;
			finalize();
			cleanup();
		} while (restart);
		
		return exit_code;
	}
	
	Game(bool is_debug = false)
	{
		Player = NULL;
		debug = is_debug;
	}
	
	~Game();
};

int main(int argc, char **argv)
{
	bool debug = false;
	if (argc == 2 && argv[1][0] == 'd') {
		debug = true;
		std::cout << "Debug mode.\n";
	}
	Game Game(debug);
	return Game.start();
}


int Game::init()
{
	PlayerChoosenRace = PlayerRace::None;
	Render.prepareMenu();
	Render.rerenderHud();
	{                          
		while (PlayerChoosenRace == PlayerRace::None) {
			Render.menu("Choose race (u/p/e)\n");
			
			if (SDL_PollEvent(&event)) {
				if (event.type == SDL_KEYDOWN) {
					switch (event.key.keysym.scancode) {
						case SDL_SCANCODE_U:
							PlayerChoosenRace = PlayerRace::Unicorn;
							break;
						case SDL_SCANCODE_P:
							PlayerChoosenRace = PlayerRace::Pegasus;
							break;
						case SDL_SCANCODE_E:
							PlayerChoosenRace = PlayerRace::Earthpony;
							break;
						default:
							continue;
					}
				} else if (event.type == SDL_QUIT) {
					return 0;
				}
				else continue;
			} else {
				SDL_Delay(100);
				continue;
			}
		}
		Render.rerenderHud();
		while (difficulty_name == "")                            //Just end my suffering...
		{                                                       //Choose difficulty and map type here..
			Render.menu("Choose difficulty (e/m/h/u)\n");
			
			if (SDL_PollEvent(&event)) {
				if (event.type == SDL_KEYDOWN) {
					switch (event.key.keysym.scancode) {
						case SDL_SCANCODE_E:
							difficulty_multiplier = 1;
							difficulty_name = "easy";
							break;
						case SDL_SCANCODE_M:
							difficulty_multiplier = 2;
							difficulty_name = "medium";
							break;
						case SDL_SCANCODE_H:
							difficulty_multiplier = 4;
							difficulty_name = "hard";
							break;
						case SDL_SCANCODE_U:
							difficulty_multiplier = 8;
							difficulty_name = "un";
							break;
						default:
							continue;
					}
				} else if (event.type == SDL_QUIT) {
					return 0;
				} else continue;
			} else {
				SDL_Delay(100);
				continue;
			}
		}
	}
	int seed = time(0);
	srand(seed);
	level = 1;
	return 1;
}

void Game::prepareGame()
{
	turn = 0;
	time_start = time(0);
	
	score_target = 5;
	
	switch (level) {
		case 1:
			Terrain.generate(40, 40, surface, &score_target);
			break;
		case 2:
			Terrain.generate(75, 75, chambers, &score_target);
			break;
		case 3:
			Terrain.generate(150, 150, tunnels, &score_target);
			break;
		case 4:
			Terrain.generate(150, 150, cave, &score_target);
			break;
		case 5:
			Terrain.generate(50, 50, bomber_lair, &score_target);
			break;
		default:
			std::cerr << "ERROR: Unknown level.\n";
	}
	if (!Player) {
		Player = new class Player(PlayerChoosenRace, &Terrain, difficulty_multiplier);
	} else {
		Player->findSpawnLocation(&Terrain);
		Player->ability = 0;
		Player->ability_state = AbilityState::Nominal;
	}
	
	if (level == 5) {
		Creature.push_back(new BomberBoss(&Terrain, difficulty_multiplier, Player));		//PLACEHOLDER AGAIN, TODO PROPER IMPLEMENTATION ETC
	} else {
		for (unsigned int counter = 0; counter < difficulty_multiplier; counter++) {
			if (level != 5) {
				Creature.push_back(new Wolf(&Terrain, difficulty_multiplier, Player));
				if (counter % 3 == 2)
					Creature.push_back(new Jumper(&Terrain, difficulty_multiplier, Player));
			}
		}
	}
}

int Game::runGame()
{
	if (SDL_PollEvent(&event)) {				//wtf is this thing
		switch (event.type) {
		case SDL_QUIT:
			input = 'e';
			run = false;
			return 0;
		case SDL_KEYDOWN:
			if (event.window.windowID == SDL_GetWindowID(Render.window)) {
				hud.clearLog();
				Render.rerenderHud();
				
				if (Player->ability_state == AbilityState::Adjusting) {		//If the Player is adjusting their ability. Trash, so much.
					if (event.key.keysym.scancode == SDL_SCANCODE_UP && Player->distanceModifier < 10) {
						Player->distanceModifier++;
					} else if (event.key.keysym.scancode == SDL_SCANCODE_DOWN && Player->distanceModifier > 2) {
						Player->distanceModifier--;
					}
					if (event.key.keysym.scancode == SDL_SCANCODE_G) input = 'g';
					else input = 0;
				} else {										//Else, normal input processing
					switch (event.key.keysym.scancode) {
						case SDL_SCANCODE_W: __attribute__ ((fallthrough));
						case SDL_SCANCODE_UP: input = 'w'; break;
						case SDL_SCANCODE_S: __attribute__ ((fallthrough));
						case SDL_SCANCODE_DOWN: input = 's'; break;
						case SDL_SCANCODE_D: __attribute__ ((fallthrough));
						case SDL_SCANCODE_RIGHT: input = 'd'; break;
						case SDL_SCANCODE_A: __attribute__ ((fallthrough));
						case SDL_SCANCODE_LEFT: input = 'a'; break;
						case SDL_SCANCODE_F: input = 'f'; break;
						case SDL_SCANCODE_G: input = 'g'; break;
						case SDL_SCANCODE_V: input = 'v'; break;
						case SDL_SCANCODE_B: input = 'b'; break;
						case SDL_SCANCODE_ESCAPE: input = 'e'; run = false; return 0;
						default:			//If there's an event that doesn't fit
							if (debug) {
								switch (event.key.keysym.scancode) {
									case SDL_SCANCODE_PERIOD:
										Player->health++;
										break;
									case SDL_SCANCODE_COMMA:
										Player->health--;
										break;
									case SDL_SCANCODE_APOSTROPHE:
										difficulty_multiplier++;
										break;
									case SDL_SCANCODE_SEMICOLON:
										difficulty_multiplier--;
										break;
									case SDL_SCANCODE_RIGHTBRACKET:
										level++;
										break;
									case SDL_SCANCODE_LEFTBRACKET:
										level--;
										break;
									default:
										return 0;	//If no event fits, debug version
								}
							} else return 0;		//If no event fits, non-debug version
					}
				}
			}
			break;
			default:		//If there's an event that doesn't fit, again...
				return 0;
		}
	} else input = 0;   //No event, continue the loop, but don't do any actions other than rendering
	
	if (input == 'v') {
		hud.addLogMessage("Framerate:\n~" + std::to_string(Render.getFramerate()) + " FPS.\n"
						+ "Delay:\n" + std::to_string(Render.getFrameTime()) + ".");
	} else if (input == 'b') {		//AD HOC, FIXME AAAAAAAAAA
		if (Player->ability == 0) {
			Creature::spawnBomb(&Creature, Player->positionX, Player->positionY, Player->distanceModifier, 5);
			Player->ability = 100;
		} else
			hud.addLogMessage("Bomb not ready.");
	} else if (input != 0)    Player->MakeTurn(&Terrain, input, &hud);
	
	if ( input == 'w' || input == 's' || input == 'a' || input == 'd' )     //Processes what should happen if a Player attempts to move.
	{                                                                       //Notice that activating/cancelling ability does not trigger it.
		const short initialHealth = Player->health;
		unsigned short addTurn = 1;             //How many turn iterations should be performed
		
		if (Player->race == PlayerRace::Pegasus && Player->ability_state == AbilityState::Used) {		//Pegasi's ability performs several turn iterations
			addTurn = (Player->distanceModifier / 2);		//Amount of turn iterations is around half of travelled distance
		}
		
		for (unsigned short turnIteration = 0; turnIteration < addTurn; turnIteration++) {					//Performs as many iterations as needed
			for (unsigned int creatureCounter = 0; creatureCounter < Creature.size(); creatureCounter++) {	//Moves creatures
				if (Creature[creatureCounter] != NULL)
					Creature[creatureCounter]->MakeTurn(&Terrain, Player, &Creature);
			}
		}
		
		if (Player->ability != 0)	//Checks if ability should cool down by one turn.
			Player->ability--;
		
		if (initialHealth > Player->health) {
			hud.addLogMessage("You have been damaged.");
		}
		Creature::reapDead(&Creature, &Terrain);
		
		Player->ability_state = AbilityState::Nominal;     //Reset to that after each "done" turn
		turn += addTurn;
	}
	
	if (input == 'e' || Player::apples >= score_target || Player->health <= 0) {
		run = false;
		return 0;
	}
	hud.setStats(Player->health, Player->distanceModifier, Player->positionX, Player->positionY, Player::apples, turn, level, Player->raceName, Player->ability);
	if (debug) hud.addHudMessage("difficulty_multiplier: " + std::to_string(difficulty_multiplier));
	if (level == 5) {
		for (unsigned int i = 0; i < Creature.size(); i++) {		//Just to be sure I guess
			if (Creature[i] != NULL && Creature[i]->type == bomber_boss)
				hud.addHudMessage("Boss health: " + std::to_string(Creature[i]->health));
		}
	}
	
	Render.gui(&Terrain, Player, Creature, hud);
	hud.clearHud();
	
	return 0;
}

void Game::finalize()
{
	hud.clearLog();
	hud.clearHud();
	int deltaTime = time(0) - time_start;									// Display time of the game
	hud.addLogMessage("Time: " + std::to_string(deltaTime / 60) + " minutes,\n"
	+ std::to_string(deltaTime % 60) + " seconds.\n"
	+ "Turns: " + std::to_string(turn) + ".\n"
	+ "Apples: " + std::to_string(Player::apples) + "/"
	+ std::to_string(score_target));
	if (Player->health <= 0 || input == 'e') {					//In case you lost or given up
		hud.addLogMessage("You have fainted (or given up).\nPress Esc to quit.\nPress R to restart.");
	} else if (Player::apples >= score_target) {					//In case you won
		hud.addLogMessage("You have collected\nenough apples.");
		if (level == 5) {										//Aaaaa stop using magic numbers (it currently means last level but aaaa) FIXME
			hud.addLogMessage("This was the last level,\nthanks for playing.");
		} else {
			hud.addLogMessage("Press R to continue\nto next level,\nor Esc to quit.");
			level++;
		}
	}
	
	Render.rerenderHud();
	Render.gui(&Terrain, Player, Creature, hud);
	hud.clearLog();
	hud.clearHud();
	
	while (true) {                      //To check if game should be repeated
		if (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) break;
			else if (event.type == SDL_KEYDOWN && event.window.windowID == SDL_GetWindowID(Render.window)) {
				if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE) break;
				else if (event.key.keysym.scancode == SDL_SCANCODE_R) {		//If the Player presses 'r'
					if (Player->health <= 0 || (input == 'e' && !debug)) {
						level = 1;
						delete Player;
						Player = NULL;
					}
					restart = true;
					if (Player)
						Player::apples = 0;
					input = 0;      //So it doesn't quit immediately afterwards. Also, trash code again.
					break;
				}
			}
			else continue;
		}
		else SDL_Delay(100);
	}
}

void Game::cleanup()						// TODO split this into two functions?
{
	for (unsigned int i = 0; i < Creature.size(); i++) {
		if (Creature[i] != NULL)
			delete Creature[i];
	}
	Creature.clear();
}

Game::~Game()
{
	for (unsigned int i = 0; i < Creature.size(); i++) {
		if (Creature[i] != NULL)
			delete Creature[i];
	}
	Creature.clear();
	if (Player) {
		delete Player;
	}
}
