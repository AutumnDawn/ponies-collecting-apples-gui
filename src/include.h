// External dependencies

#ifndef INCLUDE_H
#define INCLUDE_H

#include <iostream>
#include <fstream>
#include <ctime>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <chrono>
#ifdef WINDOWS
#define SDL_MAIN_HANDLED
#endif
#include <SDL2/SDL.h>       //I guess modify this to fit your OS'?
#include <SDL2/SDL_ttf.h>

#endif
