//Class of "wolf", which is supposed to be an enemy in the game
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "creatures.h"

#define WOLF_HEALTH 15
#define JUMPER_HEALTH 5

void Creature::spawnBomb(std::vector<Creature*> *Creature, short x, short y, short timer, short dmg)
{
	for (unsigned int i = 0; i < Creature->size(); i++) {
		if (Creature->at(i) == NULL) {
			Creature->at(i) = new Bomb(x, y, timer, dmg);
			return;
		}
	}
	Creature->push_back(new Bomb(x, y, timer, dmg));
	
}

void Creature::reapDead(std::vector<Creature*> *Creature, Terrain *Terrain)		//Yes, it's all trash
{
	for (unsigned int i = 0; i < Creature->size(); i++) {
		class Creature *cur_crt = Creature->at(i);
		if (cur_crt != NULL && cur_crt->health <= 0) {
			if (cur_crt->type == bomber_boss)
					Terrain->setBlock(cur_crt->positionX, cur_crt->positionY, Blocks::Apple);
			delete Creature->at(i);
			Creature->at(i) = NULL;
		}
	}
}

void Creature::Move(Terrain *Terrain, enum directionVar direction)
{
    short targetX = positionX;
    short targetY = positionY;
    switch (direction)              //Get the coordinates of the field it's supposed to go
    {
        case NORTH:
            targetY = positionY-1;
            targetX = positionX;
            break;
        case SOUTH:
            targetY = positionY+1;
            targetX = positionX;
            break;
        case WEST:
            targetY = positionY;
            targetX = positionX-1;
            break;
        case EAST:
            targetY = positionY;
            targetX = positionX+1;
            break;
        case STAY:
            targetY = positionY;
            targetX = positionX;
    }
	switch (Terrain->getBlockType(targetX, targetY)) {
		case Blocks::Apple:
		case Blocks::Ground:
			goto move;
		case Blocks::SoftStone:
			if (cooldown == 0) {
				Terrain->setBlock(targetX, targetY, Blocks::Ground);
				cooldown = cooldownModifier;
			}
		case Blocks::Trap:
		case Blocks::Stone:
		case Blocks::OOB:
		default: return;
	}
	move:
		positionX = targetX;
		positionY = targetY;
		return;
}

enum directionVar Creature::ChooseDirection(short targetX, short targetY)
{
    if (targetX == positionX && targetY == positionY) {   //Not sure why it's a separate thing
        return STAY;
    }
    
    if (targetX == positionX) {					//Target on same X
        if (targetY > positionY)				//Target to the south
            return SOUTH;
        else
            return NORTH;
    } else if (targetX > positionX) {			//Target is more to the east
        if (targetY > positionY) {				//Target to the south-east
            if (rand() % 2 == 0)
                return SOUTH;
            else
                return EAST;
        } else if (targetY < positionY) {		//Target to the north-east
            if (rand() % 2 == 0)
                return NORTH;
            else
                return EAST;
        } else									//Target is directly to the east
            return EAST;
    } else {									//Target is more to the west
        if (targetY > positionY) {				//Target to the south-west
            if (rand() % 2 == 0)
                return SOUTH;
            else
                return WEST;
        } else if (targetY < positionY) {		//Target to the north-west
            if (rand() % 2 == 0)
                return NORTH;
            else
                return WEST;
        } else									//Target is directly to the west
            return WEST;
    }
}

void Creature::findSpawnLocation(Terrain *Terrain, Player * Player)
{
    for (unsigned attempt = 0; attempt < 500; attempt++) {
        positionY = rand() % Terrain->terrain.size();
        positionX = rand() % Terrain->terrain[positionY].size();
        
        if (Terrain->getBlockType(positionX, positionY) == Blocks::Ground &&
            ( abs(positionX - Player->positionX) > 2 || abs(positionY - Player->positionY) > 2 )) { //So creatures don't spawn too near the Player
            return;
        }
    }
    positionY = Terrain->terrain.size() / 2;
    positionX = Terrain->terrain[positionY].size() / 2;      //If all else fails, put them in the middle
}

Creature::~Creature() {}

void Wolf::MakeTurn(Terrain *Terrain, Player * Player, std::vector<Creature*> *Creature)
{
    if (cooldown != 0)  cooldown--;
    directionVar direction = STAY;
    /*if (mage && (abs(Player->positionX-positionX) > 20 || abs(Player->positionY-positionY) > 20) && cooldown == 0) {    //To check if wolfmage should teleport
        short tempX = Player->positionX;                                                                                 //May reuse the code later, but not now
        short tempY = Player->positionY;
        
        tempX += (rand() % 21) - 10;            //"Radius" of 10 around Player
        tempY += (rand() % 21) - 10;
        
        if (positionX >= 0 && positionX < terrain[positionY].size() &&  //Wow, this condition is really ugly
            positionY >= 0 && positionY < terrain.size() &&
            terrain[tempY][tempX] == ' ' &&
            (abs(tempX - Player->positionX) >= 5 || abs(tempY - Player->positionY) >= 5))
        {
            positionX = tempX;
            positionY = tempY;
            cooldown = cooldownModifier * 7.5;  //Teleportation is tiring
            return;
        }
    }*/
    
    if (abs(positionX - Player->positionX) + abs(positionY - Player->positionY) <= 1) {   //Attack the Player if possible
        Player->health -= damage;
        return;
    } else if (abs(positionX - Player->positionX) > 20 || abs(positionY - Player->positionY) > 20) {
        switch (rand() % 4) {
            case 0:
                direction = NORTH;
                break;
            case 1:
                direction = SOUTH;
                break;
            case 2:
                direction = WEST;
                break;
            case 3:
                direction = EAST;
                break;
            default:
                direction = STAY;
        }
    } else {
        direction = ChooseDirection(Player->positionX, Player->positionY);
    }
    Move(Terrain, direction);
}

Wolf::Wolf (Terrain *Terrain, const unsigned short difficulty, Player * Player)    //Kinda messy (like everything here)
{
	health = WOLF_HEALTH;
    type = wolf;
    damage = difficulty;
    cooldown = 80 / difficulty; //To give the Player a "warm-up time"
    cooldownModifier = 16 / difficulty;
	color = {0x52, 0x37, 0x0E, 0x00};
    
    findSpawnLocation(Terrain, Player);
}

Wolf::~Wolf() {}

void Jumper::MakeTurn(Terrain *Terrain, Player * Player, std::vector<Creature*> *Creature)
{
    if (cooldown != 0) {    //Jumpers can't do anything while on cooldown
        cooldown--;
        return;
    }
    directionVar direction = STAY;
    if (abs(positionX - Player->positionX) + abs(positionY - Player->positionY) <= 1) {
            Player->health -= damage;
            cooldown = 2;
            return;
    } else if (abs(positionX - Player->positionX) > 20 || abs(positionY - Player->positionY) > 20) {
        switch (rand() % 4) {
            case 0:
                direction = NORTH;
                break;
            case 1:
                direction = SOUTH;
                break;
            case 2:
                direction = WEST;
                break;
            case 3:
                direction = EAST;
                break;
            default:
                direction = STAY;
        }
    } else {
        direction = ChooseDirection(Player->positionX, Player->positionY);
    }
    
    for (unsigned short i = 0; i < 2; i++) {    //L M A O. They jump 2 fields at once.
        Move(Terrain, direction);
    }
}

Jumper::Jumper(Terrain *Terrain, const unsigned short difficulty, Player * Player)
{
	health = JUMPER_HEALTH;
    type = jumper;
    damage = difficulty / 3;
    cooldown = 80 / difficulty;
    cooldownModifier = 16 / difficulty;
	color = {0x52, 0x0E, 0x2D, 0x00};
    
    findSpawnLocation(Terrain, Player);
}

Jumper::~Jumper() {}

void BomberBoss::spawnBombs(Terrain *Terrain, std::vector<Creature*> *Creature)
{
	for (short i = positionX; i < Terrain->terrain.size(); i += 2) {		//Boilerplate much?
		if (Terrain->getBlockType(i, positionY) == Blocks::Ground)
			spawnBomb(Creature, i, positionY, 3, damage);
		else break;
	}
	for (short i = positionX; i >= 0; i -= 2) {
		if (Terrain->getBlockType(i, positionY) == Blocks::Ground)
			spawnBomb(Creature, i, positionY, 3, damage);
		else
			break;
	}
	for (short i = positionY; i < Terrain->terrain.size(); i += 2) {
		if (Terrain->getBlockType(positionX, i) == Blocks::Ground)
			spawnBomb(Creature, positionX, i, 3, damage);
		else
			break;
	}
	for (short i = positionY; i >= 0; i -= 2) {
		if (Terrain->getBlockType(positionX, i) == Blocks::Ground)
			spawnBomb(Creature, positionX, i, 3, damage);
		else
			break;
	}
}

void BomberBoss::MakeTurn(Terrain *Terrain, Player *Player, std::vector<Creature*> *Creature)
{
	if (cooldown > 0) {
		directionVar direction = ChooseDirection(Player->positionX, Player->positionY);
		if (abs(positionX - Player->positionX) < 3 && abs(positionY - Player->positionY) < 3) {		//Move away from Player when too near
			switch (direction) {
				case NORTH: Move(Terrain, SOUTH); break;
				case SOUTH: Move(Terrain, NORTH); break;
				case WEST: Move(Terrain, EAST); break;
				case EAST: Move(Terrain, WEST); break;
				case STAY:
					health -= 5;
					if (health > 0)
						findSpawnLocation(Terrain, Player);
					break;
			}
		} else if (abs(positionX - Player->positionX) > 6 || abs(positionY - Player->positionY) > 6) {	//Move towards the Player if too far away
			Move(Terrain, direction);
		} else {							//Move randomly if in "correct" distance
			switch (rand() % 5) {			//If 0, just stay
				case 1: Move(Terrain, NORTH); break;
				case 2: Move(Terrain, SOUTH); break;
				case 3: Move(Terrain, WEST); break;
				case 4: Move(Terrain, EAST); break;
			}
		}
		if (cooldown > 0)	//Just to be sure
			cooldown--;
	} else {
		spawnBombs(Terrain, Creature);
		findSpawnLocation(Terrain, Player);
		cooldown = cooldownModifier + (rand() % 11) - 5;	//Some randomization? TODO better, maybe?
	}
	if (health < 20)
		cooldownModifier = 25;
}

BomberBoss::BomberBoss(Terrain *Terrain, const unsigned short difficulty, Player *Player)
{
	health = 50;
	type = bomber_boss;
	damage = difficulty * 1.5;
	cooldown = 10;
	cooldownModifier = 50;
	color = {0x70, 0x00, 0x2A, 0x00};
	
	findSpawnLocation(Terrain, Player);
}

BomberBoss::~BomberBoss() {}

void Bomb::MakeTurn(Terrain *Terrain, Player *Player, std::vector<Creature*> *Creature) {
	health--;
	if (health <= 0) {
		for (short y = -1; y <= 1; y++) {
			for (short x = -1; x <= 1; x++) {
				short bomb_x = positionX + x;
				short bomb_y = positionY + y;
				if (Terrain->getBlockType(bomb_x, bomb_y) != Blocks::Apple) {
					Terrain->setBlock(bomb_x, bomb_y, Blocks::Ground);
				}
				if (bomb_x == Player->positionX && bomb_y == Player->positionY)
					Player->health -= damage;
				for (unsigned int i = 0; i < Creature->size(); i++) {
					if (Creature->at(i) != NULL && Creature->at(i)->positionX == bomb_x && Creature->at(i)->positionY == bomb_y)
						Creature->at(i)->health -= damage;
				}
			}
		}
	}
}

Bomb::Bomb(short x, short y, short timer, short dmg) {
	type = bomb;
	damage = dmg;
	health = timer;
	positionX = x;
	positionY = y;
	color = {0x1D, 0x00, 0x37, 0x00};
}

Bomb::~Bomb() {}
