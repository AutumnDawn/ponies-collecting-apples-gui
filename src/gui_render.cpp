/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "gui_render.h"

#define TEMP_SIZE_X 41      //Odd, so the Player is in the middle
#define TEMP_SIZE_Y 41

#define BORDER_X 20
#define BORDER_Y 20

const SDL_Color Render::textColor = {255, 255, 255, 0};

void Render::gui(Terrain *Terrain, Player *Player,
		std::vector<Creature*> Creature, HUD &hud) {
	const int ABS_CENTER_X = Player->positionX;
	const int ABS_CENTER_Y = Player->positionY;
    
    SDL_SetRenderTarget(renderer, texture);
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(renderer);
    
	if (rerender_hud) {
		if (messageSurface != NULL)
			SDL_FreeSurface(messageSurface);
		if (message != NULL)
			SDL_DestroyTexture(message);
		
		std::string hud_s = hud.getHUD();
		messageSurface = TTF_RenderText_Blended_Wrapped(font, hud_s.c_str(), textColor, 380*scal);
		message = SDL_CreateTextureFromSurface(renderer, messageSurface);
		messageRect.h = std::count(hud_s.begin(), hud_s.end(), '\n') * 55*scal;	//55 because it fits to the text's size
		
		rerender_hud = false;
	}

	for (int y = 0; y < TEMP_SIZE_Y; y++) {				//Draw the rects
		for (int x = 0; x < TEMP_SIZE_Y; x++) {
			SDL_Color bc = Terrain->getBlockDisplayColor(ABS_CENTER_X-BORDER_X+x, ABS_CENTER_Y-BORDER_Y+y);
			SDL_SetRenderDrawColor(renderer, bc.r, bc.g, bc.b, bc.a);
			SDL_RenderDrawRect(renderer, &renderField[y][x]);
			SDL_RenderFillRect(renderer, &renderField[y][x]);
		}
	}
	{
		int ply = Player->positionY - ABS_CENTER_Y + BORDER_Y;		//Should always return center of the rendered field
		int plx = Player->positionX - ABS_CENTER_X + BORDER_X;
		SDL_Color pc = Player->color;
		SDL_SetRenderDrawColor(renderer, pc.r, pc.g, pc.b, pc.a);	// Accidental title drop
		SDL_RenderDrawRect(renderer, &renderField[ply][plx]);
		SDL_RenderFillRect(renderer, &renderField[ply][plx]);
	}
	for (unsigned int i = 0; i < Creature.size(); i++) {		//Set the creatures
		if (!(Creature[i] == NULL || abs(Creature[i]->positionY - ABS_CENTER_Y) > BORDER_Y || abs(Creature[i]->positionX - ABS_CENTER_X) > BORDER_X)) {
			short cry = Creature[i]->positionY - ABS_CENTER_Y + BORDER_Y;
			short crx = Creature[i]->positionX - ABS_CENTER_X + BORDER_X;
			float light;
			Block *block = Terrain->getBlock(Creature[i]->positionX, Creature[i]->positionY);
			if (block != NULL) {
				light = block->light;
			} else
				light = 1;
			SDL_Color cc = Creature[i]->color;
			SDL_Color cdc = cc;
			cdc.r *= light;
			cdc.g *= light;
			cdc.b *= light;
			SDL_SetRenderDrawColor(renderer, cdc.r, cdc.g, cdc.b, cdc.a);
			SDL_RenderDrawRect(renderer, &renderField[cry][crx]);
			SDL_RenderFillRect(renderer, &renderField[cry][crx]);
		}
	}
	
    SDL_SetRenderTarget(renderer, NULL);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderCopy(renderer, message, NULL, &messageRect);
    SDL_RenderPresent(renderer);
	framerate_limiter.wait();
}

//Rework of this TODO? Though knowing me, it will never happen. Well, whatever.
void Render::menu(std::string text)
{
    SDL_SetRenderTarget(renderer, texture);         //For presenting the introducing text
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(renderer);
    
	if (rerender_hud) {
		if (messageSurface != NULL)
			SDL_FreeSurface(messageSurface);
		if (message != NULL)
			SDL_DestroyTexture(message);
		
		messageSurface = TTF_RenderText_Blended_Wrapped(font, text.c_str(), textColor, 600*scal);
		message = SDL_CreateTextureFromSurface(renderer, messageSurface);
		
		rerender_hud = false;
	}
    SDL_SetRenderTarget(renderer, NULL);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderCopy(renderer, message, NULL, &messageRect);
    SDL_RenderPresent(renderer);
	framerate_limiter.wait();
}

void Render::prepareGUI() {
    for (unsigned y = 0; y < 41; y++) {
        for (unsigned x = 0; x < 41; x++) {
            renderField[y][x].w = 20*scal;
            renderField[y][x].h = 20*scal;
            renderField[y][x].x = x * 20*scal;
            renderField[y][x].y = y * 20*scal;
            
        }
    }
    messageRect.x = 820*scal;		//To the right of the "main screen", which is 820x820
    messageRect.y = 0;
    messageRect.w = 380*scal;		//How much space to the right it has
    //Rectangle's height is defined in `void Render::gui()` itself, as it depends on amount of text
    prepareHud();
}

void Render::prepareMenu() {
    messageRect.x = 300*scal;
    messageRect.y = 350*scal;		//Halfway to the down - somewhat in the middle
    messageRect.w = 600*scal;
    messageRect.h = 80*scal;		//Fits for the text's size
    prepareHud();
}

void Render::rerenderHud()
{
	rerender_hud = true;
}

float Render::getFramerate()
{
	return framerate_limiter.getFramerate();
}

float Render::getFrameTime()
{
	return framerate_limiter.getFrameTime();
}

void Render::configure()
{
    const short FPS_DEFAULT = 30;
    
    short fps_target = 30;

    std::fstream conf_f("pca-config", std::fstream::in);
    
    if (conf_f >> fps_target && conf_f >> scal) {
		if (fps_target > 0)
			framerate_limiter.setRate(fps_target);
		else
			framerate_limiter.setRate(0);
	} else {
		std::cout << "ERROR: invalid config file, (re)generating.\n";
		conf_f.close();
		conf_f.open("pca-config", std::fstream::out);
		
		conf_f << FPS_DEFAULT << "\n" << 1.0;
		framerate_limiter.setRate(FPS_DEFAULT);
		scal = 1.0;
	}
	conf_f.close();
}

Render::Render()
{
	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
#ifdef WINDOWS
	SDL_SetMainReady();
#endif
	configure();
	
	font = TTF_OpenFont("CelestiaMediumRedux_alt1.55.ttf", 40*scal);
	if (!font) {
		std::cerr << "TTF_OpenFont: " << TTF_GetError() << "\n";
		std::cerr << "Quitting...\n";
		TTF_Quit();
		SDL_Quit();
		exit(1);
	} else std::cout << "Opened the font.\n";

	window = SDL_CreateWindow("Ponies Collecting Apples",
								SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
								1200*scal, 820*scal,								//The default window is 1200x820
								SDL_WINDOW_OPENGL);
	if (!window) {
		std::cerr << "SDL_CreateWindow: " << SDL_GetError()
		<< "\nCould not create a window, quitting...\n";
		TTF_Quit();
		SDL_Quit();
		exit(1);
	} else std::cout << "Created window.\n";

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (!renderer) {
		std::cerr << "SDL_CreateRenderer: " << SDL_GetError()
		<< "\nCould not create renderer, quitting...\n";
		TTF_Quit();
		SDL_Quit();
		exit(1);
	} else std::cout << "Created renderer.\n";

	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 1200*scal, 820*scal);
	if (!texture) {
		std::cerr << "SDL_CreateTexture: " << SDL_GetError()
		<< "\nCould not create texture, quitting...\n";
		TTF_Quit();
		SDL_Quit();
		exit(1);
	} else std::cout << "Created texture.\n";
	
	messageSurface = NULL;
	message = NULL;
}

Render::~Render()
{
	if (messageSurface != NULL)
		SDL_FreeSurface(messageSurface);
	if (message != NULL)
		SDL_DestroyTexture(message);
	
	TTF_CloseFont(font);
	std::cout << "Closed font.\n";
	SDL_DestroyTexture(texture);
	std::cout << "Destroyed texture.\n";
	SDL_DestroyRenderer(renderer);
	std::cout << "Destroyed renderer.\n";
	SDL_DestroyWindow(window);
	std::cout << "Destroyed window.\n";
	
	TTF_Quit();
	std::cout << "TTF_Quit()\n";
	SDL_Quit();
	std::cout << "SDL_Quit()\n";
}

void Render::prepareHud()
{
	if (messageSurface != NULL) {
		SDL_FreeSurface(messageSurface);
		messageSurface = NULL;
	}
	if (message != NULL) {
		SDL_DestroyTexture(message);
		message = NULL;
	}
}

#undef TEMP_SIZE_X
#undef TEMP_SIZE_Y
#undef BORDER_X
#undef BORDER_Y
