// TODO OOB void light, based on ambient light?

#ifndef TERRAIN_H
#define TERRAIN_H

#include "include.h"
#include "constants.h"
#include "block.h"

enum MapType {
	surface = 1,
	chambers = 2,
	tunnels = 3,
	cave = 4,
	bomber_lair = 5
};

class Terrain {
private:
	void generateDefault();
	void generateChambers();
	void generateTunnels();
	void generateCave();
	void generateLair();
	void generateApples(int score_target);
	
	void updateLight(int x=0, int y=0, int size=0);
	void setLight(int x, int y, float light);
	
public:
	enum MapType biome;
	float ambient_light;
	std::vector<std::vector<Block> > terrain;
	Block *getBlock(int x, int y);
	Blocks getBlockType(int x, int y);
	SDL_Color getBlockColor(int x, int y);
	SDL_Color getBlockDisplayColor(int x, int y);
	bool setBlock(int x, int y, Blocks type, bool light_update = true);
	
	void generate(short SIZE_X, short SIZE_Y, enum MapType type, int *score_target);
	Terrain();
	~Terrain();
};

#endif
