/*
 * This class stores and manages the display of HUD messages, including "log" messages.
 */
#ifndef HUD_H
#define HUD_H

#include "include.h"

class HUD
{
private:
	std::string hud_message;
	std::string log_message;
public:
	void addLogMessage(std::string s);
	void addHudMessage(std::string s);
	void setStats(int health, int ad, int pos_x, int pos_y, int apples, int turn, int level, std::string race, int cd);
	std::string getHUD();
	void clearHud();
	void clearLog();
};

#endif
