/*
 * `gui` is for rendering the screen during the main game,
 * while `menu` is for rendering screen on introduction, or something
 */

#ifndef GUI_RENDER_H
#define GUI_RENDER_H

#include "include.h"
#include "player.h"
#include "terrain.h"
#include "creatures.h"
#include "hud.h"
#include "ratelimiter.h"


class Render
{
public:
	float scal;		//Defines scaling of the window and all; should probably be private?
	
    void gui(Terrain *Terrain, Player *Player,
			std::vector<Creature*> Creature, HUD &hud);

    void menu(std::string text);
    
    void prepareGUI();      //Should be used before correlating rendering function. Not every time,
    void prepareMenu();     //just after switching to the other one.
	void rerenderHud();
	
	float getFramerate();
	float getFrameTime();
	
	Render();
	~Render();
	
	SDL_Window * window;	//Needs to be public since some outside functions need it. TODO find better solution.
private:
	Ratelimiter framerate_limiter;
	SDL_Renderer * renderer;
	SDL_Texture * texture;
	TTF_Font * font;
	
    static const SDL_Color textColor;
    SDL_Surface * messageSurface;
    SDL_Texture * message;
    SDL_Rect messageRect;
    SDL_Rect renderField[41][41];   //The squares that are the representing the play field
    void configure();
    bool rerender_hud;				// Likely a temporary measure (that'll probably become permanent...)
    void prepareHud();				// This should probably have a different name. It clears `messageSurface` and `message`
};
#endif
