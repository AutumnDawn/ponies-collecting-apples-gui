//Player class.
/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "player.h"

#define MAX_ABILITY_DISTANCE 10     //The maximum value you can set for special ability

#define BLOCKED "You cannot go\nany further."
#define COLLECTED "Apple collected."
#define DESTROYED "Obstacle destroyed."

int Player::apples = 0;

void Player::Move(Terrain *Terrain, HUD *hud,
                  enum directionVar direction, unsigned short modifier = 1)
{
    short targetX = positionX;
    short targetY = positionY;
    switch (direction) {            //Get the coordinates of the field it's supposed to go
        case NORTH:
            targetY = positionY-modifier;
            targetX = positionX;
            break;
        case SOUTH:
            targetY = positionY+modifier;
            targetX = positionX;
            break;
        case WEST:
            targetY = positionY;
            targetX = positionX-modifier;
            break;
        case EAST:
            targetY = positionY;
            targetX = positionX+modifier;
            break;
        case STAY: __attribute__ ((fallthrough));
        default:
            targetY = positionY;
            targetX = positionX;
    }
    
    
	switch (Terrain->getBlockType(targetX, targetY)) {
		case Blocks::Apple:
			apples++;
			Terrain->setBlock(targetX, targetY, Blocks::Ground);
			hud->addLogMessage(COLLECTED);
		case Blocks::Ground:
			goto move;
		case Blocks::Stone:
			if (race == PlayerRace::Earthpony && ability == 0 && ability_state == AbilityState::Nominal)
			{
				ability = cooldownModifier;
				Terrain->setBlock(targetX, targetY, Blocks::Ground);
				hud->addLogMessage(DESTROYED);
				if (rand() % 100 == 0) {
					health--;
					hud->addLogMessage("You have hurt\nyourself while\nbreaking the stone.");
				}
				return;
			}
			goto blocked;
		case Blocks::SoftStone:
			if (ability == 0 && ability_state == AbilityState::Nominal) {	//No digging with abilities
				if (race != PlayerRace::Earthpony) ability = 3;				//So the digging is "slower"
				Terrain->setBlock(targetX, targetY, Blocks::Ground);
				hud->addLogMessage(DESTROYED);
			}
			return;
			
		case Blocks::Trap:
			if (race == PlayerRace::Earthpony) {
				Terrain->setBlock(targetX, targetY, Blocks::Ground);
				hud->addLogMessage(DESTROYED);
			} else {
				health = 0;
			}
			return;
			
		default: goto blocked;
	}
	blocked:
		if (race != PlayerRace::Pegasus) {			//Ad hoc kinda???
			hud->addLogMessage(BLOCKED);
			if (ability_state == AbilityState::Used) {
				ability = 0;
				ability_state = AbilityState::Nominal;
			}
		}
		return;
	move:
		positionX = targetX;
		positionY = targetY;
	return;
}
        
void Player::Ability(HUD *hud) //Processes ability activation/deactivation
{
	if (ability == 0) {
		if (ability_state == AbilityState::Nominal) {
			ability_state = AbilityState::Active;
			hud->addLogMessage("Your ability is\nnow activated and\nwill be applied\nwith your next move.");
		} else if (ability_state == AbilityState::Active) {
			ability_state = AbilityState::Nominal;
			hud->addLogMessage("Cancelled.");
		} else {
			hud->addLogMessage("For some reason\nability_state seems to be in a state\nthat shouldn't be possible right now.");
		}
	} else
		hud->addLogMessage("Ability not ready.");
}
        
void Player::Adjust(HUD *hud)
{
    if (race == PlayerRace::Earthpony) {
        hud->addLogMessage("Your ability\nisn't adjustable.");
    } else if (ability_state == AbilityState::Adjusting) {
        hud->addLogMessage("Cancelled adjusting.");
        ability_state = AbilityState::Nominal;
    } else if (ability_state == AbilityState::Nominal) {
        hud->addLogMessage("Press up-arrow\nto increase by 1,\npress down-arrow\nto decrease by 1.\nPress G again\nto finish.");
        ability_state = AbilityState::Adjusting;
    } else {
		hud->addLogMessage("You can't do that now.");
	}
}

void Player::DecideAction(char action, enum directionVar *direction, HUD *hud) {
    switch (action) { 
        case 'f':
            Ability(hud);
            break;
        case 'g':
            Adjust(hud);
            break;
        case 'w': *direction = NORTH; return;
        case 's': *direction = SOUTH; return;
        case 'a': *direction = WEST; return;
        case 'd': *direction = EAST; return;
    }
    *direction = STAY;
}

void Player::findSpawnLocation(Terrain *Terrain) {
    for (unsigned counter = 0; counter < 500; counter++) //If spawning Player fails after X attempts, give up to prevent neverending loop
    {
        int y = rand() % Terrain->terrain.size();
        int x = rand() % Terrain->terrain[y].size();
        if (Terrain->getBlockType(x, y) == Blocks::Ground) {
            positionX = x;
            positionY = y;
            break;
        }
    }
}

void Player::MakeTurn(Terrain *Terrain, char action, HUD *hud)
{
	enum directionVar direction;
	DecideAction(action, &direction, hud);
	if (direction == STAY)
		return;
	unsigned short modifier = 1;
	if (ability_state == AbilityState::Active) {
		switch (race) {
			case PlayerRace::Unicorn:
				modifier = distanceModifier;
				ability = cooldownModifier * distanceModifier;  //Unis' cooldown is affected by distance and difficulty
				break;
			case PlayerRace::Pegasus:
				for (unsigned short i = 0; i < distanceModifier; i++) {
					Move(Terrain, hud, direction);
				}
				ability = cooldownModifier;
				break;
			case PlayerRace::Earthpony:
				Move(Terrain, hud, direction);  //Ehh...
				ability = cooldownModifier;
				break;
			default:
				hud->addLogMessage("This race has no active ability.\n");
		}
		ability_state = AbilityState::Used;
	}
	Move(Terrain, hud, direction, modifier);
}

Player::Player(PlayerRace race, Terrain *Terrain, const unsigned short difficulty_multiplier)
{
	findSpawnLocation(Terrain);
	ability = 0;
	ability_state = AbilityState::Nominal;
	this->race = race;
	apples = 0;
	switch (race) {
		case PlayerRace::Unicorn:
			distanceModifier = 3;
			cooldownModifier = UNI_COOLDOWN * difficulty_multiplier;
			health = 3;
			raceName = "Unicorn";		//Could be better I guess? This thing shouldn't even exist.
			color = {0xFF, 0xEE, 0xDD, 0x00};
			break;
		case PlayerRace::Pegasus:
			distanceModifier = 3;
			cooldownModifier = PEGA_COOLDOWN * difficulty_multiplier;
			health = 3;
			raceName = "Pegasus";
			color = {0xFF, 0xEE, 0xDD, 0x00};
			break;
		case PlayerRace::Earthpony:
			distanceModifier = 2;
			cooldownModifier = EP_COOLDOWN * difficulty_multiplier;
			health = 5;
			raceName = "Earthpony";
			color = {0xFF, 0xEE, 0xDD, 0x00};
			break;
		default:					//Fallback?
			health = 3;
			raceName = "Unknown";
			cooldownModifier = 9999;
			distanceModifier = 3;
			color = {0xFF, 0x00, 0xFF, 0x00};
	}
}

Player::~Player() {}

#undef MAX_ABILITY_DISTANCE
#undef BLOCKED
#undef COLLECTED
#undef DESTROYED
