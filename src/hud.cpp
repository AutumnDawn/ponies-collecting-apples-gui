#include "hud.h"

using namespace std;		// I'm seriously too lazy to type std:: so many times for this one time

void HUD::addLogMessage(std::string s)
{
	log_message += s + "\n";
}

void HUD::addHudMessage(std::string s)
{
	hud_message += s + "\n";
}

void HUD::setStats(int health, int ad, int pos_x, int pos_y, int apples, int turn, int level, std::string race, int cd)
{
	hud_message.append("Health: " + to_string(health) + "\n");
	hud_message.append("Ability distance: " + to_string(ad) + "\n");
	hud_message.append("Position: " + to_string(pos_x) + " " + to_string(pos_y) + "\n");
	hud_message.append("Apples: " + to_string(apples) + "\n");
	hud_message.append("Turn: " + to_string(turn) + "\n");
	hud_message.append("Level: " + to_string(level) + "\n");
	hud_message.append("Race: " + race + "\n");
	if (cd > 0)
		hud_message.append("Cooldown: " + to_string(cd) + "\n");
}

std::string HUD::getHUD()
{
	return hud_message + log_message;
}

void HUD::clearHud()
{
	hud_message.clear();
}

void HUD::clearLog()
{
	log_message.clear();
}
