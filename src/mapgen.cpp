/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Continuation of terrain.cpp; includes all the map generation-related functions/methods

#include "terrain.h"

void Terrain::generateApples(int score_target) {
	short x;
	short y;
	unsigned short ag = 0;
	unsigned short attempt = 0;


	while (ag < score_target && attempt < 1000) //Create apples, 1000 error tolerance
	{
		y = rand() % terrain.size();
		x = rand() % terrain[y].size();
		
		if (getBlockType(x, y) == Blocks::Ground || ( attempt > 900 && getBlockType(x, y) != Blocks::Apple)) {	//If attempts exceed 900,
																												//try placing them everywhere
			setBlock(x, y, Blocks::Apple, false);
			ag++;
		}
		else {
			attempt++;
		}
	}
}


void Terrain::generateDefault () {
	const short SIZE_X = terrain.size();
	const short SIZE_Y = terrain[0].size();
    for (short y = 0; y < SIZE_Y; y++) {
        for (short x = 0; x < SIZE_X; x++) {
            if (rand() % 60 == 0) {
                short tempX = x;
                short tempY = y;
                for (unsigned short counter = 0; counter < 100; counter++) {			//Generates a "lump" of rock and stone
                    if (tempX >= 0 && tempX < SIZE_X && tempY >= 0 && tempY < SIZE_Y)
                    {
                        setBlock(tempX, tempY, Blocks::Stone, false);
                        tempX += (rand() % 3) - 1;
                        tempY += (rand() % 3) - 1;
                    } else
                        break;
                }
            } else if (rand() % 40 == 0)
                setBlock(x, y, Blocks::Trap, false);
            else
                setBlock(x, y, Blocks::Ground, false);
        }
    }
}


void Terrain::generateChambers () {
	const short SIZE_X = terrain.size();
	const short SIZE_Y = terrain[0].size();
	unsigned short chamberSize = 1;
	const unsigned attemptLimit = (SIZE_X * SIZE_Y) / 100;
	short chamberX;
	short chamberY;
	enum Blocks fill_with = Blocks::Ground;		//What the chamber should be filled with

	for (short y = 0; y < SIZE_Y; y++) {		//Fill everything with soft stone
		for (short x = 0; x < SIZE_X; x++) {
			setBlock(x, y, Blocks::SoftStone, false);
		}
	}
	
	for (unsigned attempt = 0; attempt < attemptLimit; attempt++) {
		if (rand() % 25 == 0)   fill_with = Blocks::Trap;
		else                    fill_with = Blocks::Ground;
		
		chamberX = (rand() % (SIZE_X - 8)) + 4;               //Find random place for the chamber
		chamberY = (rand() % (SIZE_Y - 8)) + 4;
		
		switch (rand() % 10)
		{
			case 0:                                             //10% chance for "big" chamber
				chamberSize = 3;
				break;
			case 1:                                             //20% for "moderate-size" chamber
			case 2:
				chamberSize = 2;
				break;
			default:                                            //70% for "small" chamber
				chamberSize = 1;
		}
		
		for (short drillY = chamberY - chamberSize; drillY <= chamberY + chamberSize; drillY++)
		{
			for (short drillX = chamberX - chamberSize; drillX <= chamberX + chamberSize; drillX++)
			{
				setBlock(drillX, drillY, fill_with, false);
			}
		}
	}
}


void Terrain::generateTunnels() {
	const short SIZE_X = terrain.size();
	const short SIZE_Y = terrain[0].size();
	unsigned short direction = rand() % 4 + 1;  //Defines where the "digger" goes
	short diggerX = SIZE_X / 2;             //Start somewhere in the middle
	short diggerY = SIZE_Y / 2;

	enum Blocks field_fill = Blocks::Ground;		//Defines what digger should fill the field with
	const unsigned int attemptMax = (SIZE_X * SIZE_Y) / 40;


	for (short y = 0; y < SIZE_Y; y++) {			//Everything is rock by default
		for (short x = 0; x < SIZE_X; x++) {
			setBlock(x, y, Blocks::Stone, false);
		}
	}

	for (unsigned int attempt = 0; attempt < attemptMax; attempt++) {
		if (rand() % 50 == 0)						//2% chance for field to be filled with traps
			field_fill = Blocks::Trap;
		else
			field_fill = Blocks::Ground;
		
		for (short y = diggerY - 1; y <= diggerY + 1; y++) {	//Clears a 3x3 area around the "digger"
			for (short x = diggerX - 1; x <= diggerX + 1; x++) {
				setBlock(x, y, field_fill, false);
			}
		}
		
		if ((diggerY < 6 || diggerY > SIZE_Y - 7) &&	//If in the corner,
			(diggerX < 6 || diggerX > SIZE_X - 7)) {	//go back to center, because I'm lazy
			diggerX = SIZE_X / 2;
			diggerY = SIZE_Y / 2;
		}
		else if (diggerX < 6) {							//Else, if near the edge, find another direction
			switch(rand() % 3) {
				case 0:
					direction = NORTH;
					break;
				case 1:
					direction = SOUTH;
					break;
				case 2:
					direction = EAST;
			}
		}
		else if (diggerX > SIZE_X - 7) {
			switch(rand() % 3) {
				case 0:
					direction = NORTH;
					break;
				case 1:
					direction = SOUTH;
					break;
				case 2:
					direction = WEST;
			}
		}
		else if (diggerY < 6) {
			switch(rand() % 3) {
				case 0:
					direction = WEST;
					break;
				case 1:
					direction = EAST;
					break;
				case 2:
					direction = SOUTH; 
			}
		}
		else if (diggerY > SIZE_Y - 7) {
			switch(rand() % 3) {
				case 0:
					direction = WEST;
					break;
				case 1:
					direction = EAST;
					break;
				case 2:
					direction = NORTH; 
			}
		}
		
		switch (direction) {	//Decides where to go based on direction
			case NORTH:
				diggerY -= 3;
				break;
			case SOUTH:
				diggerY += 3;
				break;
			case WEST:
				diggerX -= 3;
				break;
			case EAST:
				diggerX += 3;
		}
		
		if (rand() % 20 == 0 || attempt % 20 == 0)  //Reroll direction, if walked too long in one direction,
		{                                       //or random chance
			direction = rand() % 4 + 1;
		}
	}
}


void Terrain::generateCave() {
	/*Using "temporary" vectors, so the changes applied to it don't interfere with the real thing */
	std::vector<std::vector<enum Blocks> > tempTerrain(terrain.size(), std::vector<enum Blocks>(terrain[0].size()));		//What will be the "base"
	std::vector<std::vector<enum Blocks> > ghostTerrain(terrain.size(), std::vector<enum Blocks>(terrain[0].size()));		//What will be "acted upon"
	const unsigned MOVES = (terrain[0].size() * terrain.size()) / 250;
	int tun_y = terrain.size() / 2;			//The "tunneler". In the middle, just like in tunnels map, because I'm lazy.
	int tun_x = terrain[tun_y].size() / 2;
	enum directionVar direction_x;					//Decides where tunneler wants to go
	enum directionVar direction_y;
	
	switch (rand() % 3) {
		case 0:
			direction_x = WEST; break;
		case 1:
			direction_x = EAST; break;
		case 3:
			direction_x = STAY; break;
	}
	switch (rand() % (direction_x != STAY ? 3 : 2)) {			//AAAAAAAAAAAAAAAAAA
		case 0:
			direction_y = NORTH; break;
		case 1:
			direction_y = SOUTH; break;
		case 3:
			direction_y = STAY; break;
	}
	
	for (int y = 0; y < terrain.size(); y++) {				//Set the entire array to ground, except at the boundaries, which is stone
		for (int x = 0; x < terrain[y].size(); x++) {
			setBlock(x, y, Blocks::Stone, false);
			tempTerrain[y][x] = Blocks::Stone;
			ghostTerrain[y][x] = Blocks::Stone;
		}
	}
	
	for (unsigned i = 0; i < MOVES; i++) {		//How many times the "tunneler" moves
		tempTerrain[tun_y][tun_x] = Blocks::Ground;
		for (unsigned j = 0; j < 8; j++) {		//How many times the "automaton" iterates (per move)
			for (int y = 0; y < terrain.size(); y++) {		//"Main" coordinates?? I guess?
				for (int x = 0; x < terrain[y].size(); x++) {
					unsigned short ground_cnt = 0;
					
					for (short ly = -1; ly <= 1; ly++) {		//"Local" coordinates
						for (short lx = -1; lx <= 1; lx++) {
							if (y + ly < 0 || y + ly >= terrain.size() || x + lx < 0 || x + lx >= terrain[y+ly].size())
								continue;
							
							if (tempTerrain[y + ly][x + lx] == Blocks::Ground)
								ground_cnt++;
						}
					}
					if (ground_cnt > 0 && ground_cnt <= 3) {
						if (rand() % 4 == 0)
							ghostTerrain[y][x] = Blocks::Ground;
					} else if (ground_cnt > 3 && ground_cnt <= 6) {
						if (rand() % 2 == 0)
							ghostTerrain[y][x] = Blocks::Ground;
					} else if (ground_cnt > 6) {
						ghostTerrain[y][x] = Blocks::Ground;
					}
				}
			}
			for (int ly = 0; ly < terrain.size(); ly++) {		//Apply the changes to the temp terrain
				for (int lx = 0; lx < terrain[ly].size(); lx++) {
					tempTerrain[ly][lx] = ghostTerrain[ly][lx];
				}
			}
		}
		for (int y = 0; y < terrain.size(); y++) {
			for (int x = 0; x < terrain[y].size(); x++) {
				if (tempTerrain[y][x] == Blocks::Ground)
					setBlock(x, y, Blocks::Ground, false);
				tempTerrain[y][x] = Blocks::Stone;
				ghostTerrain[y][x] = Blocks::Stone;
			}
		}
		switch (direction_x) {				//Things about moving the tunneler start here
			case WEST:
				tun_x -= (rand() % 5) + 1;
				break;
			case EAST:
				tun_x += (rand() % 5) + 1;
				break;
		}									//THERE IS NO DEFAULT; FFS COMPILER, STOP COMPLAINING, IT'S INTENTIONAL
		switch (direction_y) {
			case NORTH:
				tun_y -= (rand() % 5) + 1;
				break;
			case SOUTH:
				tun_y += (rand() % 5) + 1;
				break;
		}
		if (tun_y < 0 || tun_y >= terrain.size() || tun_x < 0 || tun_x >= terrain[tun_y].size()) {		//AAAAAAAAAAAAAAAAAA anti pattern aaaa; TODO fix
			tun_y = terrain.size() / 2;
			tun_x = terrain[tun_y].size() /2;
		}
		if (rand() % 8 == 0) {			//This surely must be something bad, this code is trash
			switch (rand() % 3) {
				case 0:
					direction_x = WEST; break;
				case 1:
					direction_x = EAST; break;
				case 2:
					direction_x = STAY; break;
			}
			switch (rand() % (direction_x != STAY ? 3 : 2)) {
				case 0:
					direction_x = WEST; break;
				case 1:
					direction_x = EAST; break;
				case 2:
					direction_x = STAY; break;
			}
		}
	}
	{				// AHAHAHAHA this is literally copied from applegen because I'm lazy xD TODO replace it with something that's not such a placeholder crap
		short x;
		short y;
		unsigned short lanternsGenerated = 0;
		unsigned short attempt = 0;

		while (lanternsGenerated < 10 && attempt < 1000) //Create lanters, 1000 error tolerance
		{
			y = rand() % terrain.size();
			x = rand() % terrain[y].size();
			
			if (getBlockType(x, y) == Blocks::Ground || ( attempt > 900 && getBlockType(x, y) != Blocks::Apple)) {	//If attempts exceed 900,
																													//try placing them everywhere
				setBlock(x, y, Blocks::Lantern, false);
				lanternsGenerated++;
			}
			else {
				attempt++;
			}
		}
	}
}


void Terrain::generateLair()
{
	for (int y = 0; y < terrain.size(); y++) {			//Clear the terrain
		for (int x = 0; x < terrain[y].size(); x++) {
			setBlock(x, y, Blocks::Stone, false);
		}
	}
	
	for (int i = 0; i < 25; i++) {					//That number because uhh, good question
		int rock_y = rand() % terrain.size();
		int rock_x = rand() % terrain[rock_y].size();
		int rock_size = (rand() % 4) + 2;		//Describes "radius" of the rock, in each direction. Min 2, max 5.
		
		for (int tmp_y = -1 * rock_size; tmp_y <= rock_size; tmp_y++) {
			for (int tmp_x = -1 * rock_size; tmp_x <= rock_size; tmp_x++) {
				setBlock(rock_x+tmp_x, rock_y+tmp_y, Blocks::Ground, false);
			}
		}
	}
}
