#include "terrain.h"

#define LIGHT_FALLOFF 4
#define THRESHOLD 0.09			// Light level past which it's counted as 0. Should (but doesn't have to) be rootable.

Block *Terrain::getBlock(int x, int y)
{
	if (y < 0 || y >= this->terrain.size() || x < 0 || x >= this->terrain[y].size()) {
		return NULL;
	}
	return &(this->terrain[y][x]);
}

Blocks Terrain::getBlockType(int x, int y)
{
	if (y >= 0 && y < this->terrain.size() && x >= 0 && x < this->terrain[y].size()) {
		return terrain[y][x].type;
	} else
		return Blocks::OOB;
}

SDL_Color Terrain::getBlockColor(int x, int y)
{
	if (y >= 0 && y < this->terrain.size() && x >= 0 && x < this->terrain[y].size()) {
		return terrain[y][x].color;
	} else
		return {0x19, 0x00, 0x37, 0x00};		// Stupid constant, FIXME
}

SDL_Color Terrain::getBlockDisplayColor(int x, int y)
{
	if (y >= 0 && y < this->terrain.size() && x >= 0 && x < this->terrain[y].size()) {
		return terrain[y][x].display_color;
	} else
		return {0x19, 0x00, 0x37, 0x00};		// Copied, lol.
}

bool Terrain::setBlock(int x, int y, enum Blocks type, bool light_update)
{
	Block *block = getBlock(x, y);
	if (block == NULL)
		return 0;
	Block old_block = *block;
	block->setType(type);
	if (light_update && (old_block.light_source || block->light_source)) {
		/*int use_light;
		if (old_block.light_emission > block->light_emission) {
			use_light = old_block.light_emission;
		} else {
			use_light = block->light_emission;
		}
		int light_update_size = LIGHT_FALLOFF*( sqrt(use_light/THRESHOLD)-1 );		// Don't ask where this came from, math has been done
		updateLight(x, y, light_update_size);*/
		updateLight();			// I'm too stupid to do such optimization. Be happy it's not Javascript, at least.
	}
	return 1;
}

void Terrain::setLight(int x, int y, float light)
{
	if (y < 0 || y >= this->terrain.size() || x < 0 || x >= this->terrain[y].size()) {
		return;
	}
	Block *tmp = &(this->terrain[y][x]);
	tmp->light = light;
	tmp->updateLight();
}

void Terrain::updateLight(int x, int y, int size)
{
	if (size < 0) {
		return;
	}
	std::vector<std::vector<int> > light_sources;		// Detect all the light sources within the update region. 0 is x, 1 is y.
	if (size == 0) {									// This probably isn't how it's supposed to look like.
		y = this->terrain.size()/2 - 1;
		x = this->terrain[0].size()/2 - 1;
		size = this->terrain.size() > this->terrain[0].size() ? this->terrain.size() : this->terrain[0].size();
		size = size/2 + 1;
	}
		
	for (int iy = y-size; iy <= y+size; iy++) {
		for (int ix = x-size; ix <= x+size; ix++) {
			Block *tmp = getBlock(ix, iy);
			if (tmp != NULL && tmp->light_source) {
				light_sources.push_back({ix, iy});
			}
		}
	}
	for (int iy = y-size; iy <= y+size; iy++) {
		for (int ix = x-size; ix <= x+size; ix++) {
			if (getBlockType(ix, iy) == Blocks::OOB)
				continue;
			float light = this->ambient_light;
			for (int i = 0; i < light_sources.size(); i++) {		// This is where the math happens
				float distance = sqrt((ix-light_sources[i][0])*(ix-light_sources[i][0]) + (iy-light_sources[i][1])*(iy-light_sources[i][1]));
				Block *tmp_block = getBlock(light_sources[i][0], light_sources[i][1]);
				float light_level = tmp_block->light_emission;
				float add_light = light_level/( ((distance/LIGHT_FALLOFF)+1)*((distance/LIGHT_FALLOFF)+1) );
				light += add_light;
			}
			if (light <= THRESHOLD)
				light = 0;
			else if (light > 1)
				light = 1;
			setLight(ix, iy, light);
		}
	}
}

void Terrain::generate(short SIZE_X, short SIZE_Y, enum MapType type, int *score_target) {
	terrain.resize(SIZE_Y);
	for (short i = 0; i < SIZE_Y; i++) {
		terrain[i].resize(SIZE_X, Block(Blocks::Unknown));
	}
	biome = type;
	bool noApples = false;
	ambient_light = 1;
	switch (biome) {
		case surface:
			generateDefault();
			*score_target = 5;
			break;
		case chambers:
			generateChambers();
			*score_target = 10;
			break;
		case tunnels:
			generateTunnels();
			*score_target = 15;
			break;
		case cave:
			generateCave();
			*score_target = 15;
			ambient_light = 0;
			break;
		case bomber_lair:
			generateLair();
			*score_target = 1;
			noApples = true;		//Apple is dropped by the boss
	}
	if (!noApples)
		generateApples(*score_target);
	updateLight();
}

Terrain::Terrain() {}

Terrain::~Terrain() {
	for (short i = 0; i < terrain.size(); i++) {
		terrain[i].clear();
	}
	terrain.clear();
}
