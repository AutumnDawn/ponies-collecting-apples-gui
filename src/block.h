#ifndef BLOCK_H
#define BLOCK_H

#include "include.h"

enum class Blocks
{
	Ground,
	Stone,
	SoftStone,
	Apple,
	Trap,
	Lantern,
	Unknown,
	OOB			// "Out of bounds"; not a real block, but used as a placeholder for "space" outside terrain
};



class Block
{
public:
	enum Blocks type;
	void setType(enum Blocks type);
	void updateLight();
	
	bool light_source;
	float light_emission;				// Light emitter by the block, if applicable
	SDL_Color color;
	float light;						// Light falling on the block
	SDL_Color display_color;			// The color of the object, with light applied
	Block(Blocks type);
};

#endif
