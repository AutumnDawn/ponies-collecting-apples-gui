#include "ratelimiter.h"

Ratelimiter::Ratelimiter()
{
	time_old = std::chrono::steady_clock::now();
}

void Ratelimiter::setRate(int newrate)
{
	if (newrate == 0)
		delay = 0;
	else if (newrate <= 0)
		std::cerr << "Ratelimiter: rate must be positive\n";
	else
		delay = 1000.0/newrate;
}

void Ratelimiter::wait()
{
	time_new = std::chrono::steady_clock::now();
	frame_time = std::chrono::duration_cast<std::chrono::milliseconds>(time_new - time_old);
	if (frame_time.count() < delay) {
		SDL_Delay(delay - frame_time.count());
		real_frame_time = delay;
	} else
		real_frame_time = frame_time.count();
	time_old = time_new;
}

float Ratelimiter::getFramerate()
{
	return 1000.0/real_frame_time;
}

float Ratelimiter::getFrameTime()
{
	return real_frame_time;
}
