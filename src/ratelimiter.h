#ifndef RATELIMITER_H
#define RATELIMITER_H
#include "include.h"

/* Name of this class should be self-explanatory
 * delay is the delay between cycles/frames
 * time_old is the time of the previous check
 * frame_time is how long the frame actually took to complete
 * real_frame_time is how long it took to finish, including added delay
 * 
 * setRate sets the desired rate /s. Can be passed 0 to set unlimited rate.
*/

class Ratelimiter
{
private:
	double delay;
	std::chrono::steady_clock::time_point time_new;
	std::chrono::steady_clock::time_point time_old;
	std::chrono::milliseconds frame_time;
	float real_frame_time;
public:
	void setRate(int newrate);
	void wait();
	float getFramerate();
	float getFrameTime();
	
	Ratelimiter();
};

#endif
