/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef PLAYER_H
#define PLAYER_H

#include "include.h"
#include "constants.h"
#include "block.h"
#include "terrain.h"
#include "hud.h"

#define MESSAGE std::string &afterturnMessage

enum class PlayerRace{None, Unicorn, Pegasus, Earthpony};

enum class AbilityState{Nominal, Active, Used, Adjusting};		//I should probably move "Adjusting" away or something? To separate bool? TODO???
																//It resets to "Nominal" after each "done" turn, that is, when things actually happen
class Player
{
protected:
    void Move(Terrain *Terrain, HUD *hud,
              enum directionVar direction, unsigned short modifier);
    void Ability(HUD *hud);              //For using abilities
    void Adjust(HUD *hud);               //For adjusting abilities
    void DecideAction(char action, enum directionVar *direction, HUD *hud);        //Decides what to do based on `action`. Mostly just direction.
    
public:
    enum PlayerRace race;               //Defines species/race
    std::string raceName;               //Full name of the race
    SDL_Color color;
    
    short positionX;            //Defines Player's position
    short positionY;
    
    unsigned short ability;         	//Defines cooldown; for stupidity reasons, a nonindicative name
    unsigned short distanceModifier;    //Defined by Player input
    unsigned short cooldownModifier;    //Defined by race and difficulty
    enum AbilityState ability_state;
    int health;
	static int apples;
    
	void findSpawnLocation(Terrain *Terrain);
    void MakeTurn(Terrain *Terrain, char action, HUD *hud);
	Player(PlayerRace race, Terrain *Terrain, const unsigned short difficulty_multiplier);
    ~Player();
};

#undef MESSAGE
#endif
