/*
    Copyright (C) 2020  Autumn Dawn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CREATURES_H
#define CREATURES_H

#include "include.h"
#include "constants.h"
#include "block.h"
#include "terrain.h"
#include "player.h"

enum creatureType {
	wolf,
	jumper,
	bomber_boss,
	bomb
};

class Creature      //Base class for all the creatures, I guess
{
protected:
    short damage;
    void Move(Terrain *Terrain, enum directionVar direction);  //Used every time the wolf should move
    enum directionVar ChooseDirection(short targetX, short targetY);
    
    void findSpawnLocation(Terrain *Terrain, Player * Player);

public:
    enum creatureType type;
    short positionX;
    short positionY;
	short health;			//In case of bomb: if hits 1, "explodes". For other creatures, not used (yet).
    unsigned short cooldownModifier;
    unsigned short cooldown;
	SDL_Color color;
	
    virtual void MakeTurn(Terrain *Terrain, Player * Player, std::vector<Creature*> *Creatures) = 0;
    virtual ~Creature();
	
	static void spawnBomb(std::vector<Creature*> *Creature, short x, short y, short timer, short dmg);		/* AAAAAAA should make universal for all the creatures. FIXME
																											   Maybe use some `static void spawnCreature() as a separate one */
	static void reapDead(std::vector<Creature*> *Creature, Terrain *Terrain);
};
    

class Wolf: public Creature
{
public:
    void MakeTurn(Terrain *Terrain, Player * Player, std::vector<Creature*> *Creature);
    
    Wolf(Terrain *Terrain, const unsigned short difficulty, Player * Player);
    ~Wolf();
};

class Jumper: public Creature
{
public:
    void MakeTurn(Terrain *Terrain, Player * Player, std::vector<Creature*> *Creature);
    
    Jumper(Terrain *Terrain, const unsigned short difficulty, Player * Player);
    ~Jumper();
};

class BomberBoss: public Creature
{
private:
	void spawnBombs(Terrain *Terrain, std::vector<Creature*> *Creature);
public:
	void MakeTurn(Terrain *Terrain, Player *Player, std::vector<Creature*> *Creature);
	
	BomberBoss(Terrain *Terrain, const unsigned short difficulty, Player *Player);
	~BomberBoss();
};

class Bomb: public Creature
{
public:
	void MakeTurn(Terrain *Terrain, Player *Player, std::vector<Creature*> *Creature);
	
	Bomb(short x, short y, short timer, short dmg);
	~Bomb();
};

#endif
