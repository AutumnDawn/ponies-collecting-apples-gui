#include "block.h"

void Block::setType(enum Blocks type)
{
	this->type = type;
	light_source = false;
	light_emission = 0;
	switch (this->type) {		// Bleh. TODO implement this better, somehow.
		case Blocks::Ground:	color = {0x64, 0x64, 0x64, 0x00}; break;
		case Blocks::Stone:		color = {0x2C, 0x2C, 0x2C, 0x00}; break;
		case Blocks::SoftStone:	color = {0x2A, 0x18, 0x3C, 0x00}; break;
		case Blocks::Apple:		color = {0x36, 0x9E, 0x37, 0x00}; break;
		case Blocks::Trap:		color = {0x8A, 0x2F, 0x38, 0x00}; break;
		case Blocks::Lantern:	color = {0xFA, 0xE6, 0xA0, 0x00}; break;
		case Blocks::OOB:		color = {0x19, 0x00, 0x37, 0x00}; break;		// This... shouldn't be used?
		default:				color = {0xFF, 0x00, 0xFF, 0x00}; break;
	}
	if (this->type == Blocks::Lantern) {	// Probably can make this better?
		light_source = true;
		light_emission = 1;
	}
	updateLight();	
}

void Block::updateLight()
{
	if (this->type == Blocks::Apple && light < 0.4) {		// I'm merciful, therefore apples shall never visually appear below light level 0.4
		display_color.r = color.r * 0.4;
		display_color.g = color.g * 0.4;
		display_color.b = color.b * 0.4;
	} else {
		display_color.r = color.r * light;
		display_color.g = color.g * light;
		display_color.b = color.b * light;
	}
}

Block::Block(Blocks type)
{
	setType(type);
}
