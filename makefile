#I have no idea how to make one for anything that's not Linux

CC = g++
mingw = x86_64-w64-mingw32-g++
CCC = clang++
CFLAGS = -Wall -Wextra -std=c++11 -pedantic
LFLAGS = `sdl2-config --libs` -lSDL2_ttf -lstdc++ -lc
src = src/*.cpp

objs = *.o

objs: $(src)
	$(CC) $(CFLAGS) `sdl2-config --cflags` -c ${src}

pca: $(objs)
	$(CC) -o ./pca $(objs) $(LFLAGS) -s

objs-clang: ${src}
	${CCC} $(CFLAGS) `sdl2-config --cflags` -c ${src}

pca-clang: $(objs)
	$(CCC) -o ./pca $(objs) $(LFLAGS) -s

debug: $(objs)
	$(CCC) -o ./pca $(objs) $(LFLAGS) -g

pca.exe: $(src)
	$(mingw) $(src) $(CFLAGS) -o ./pca.exe -I. -D_REENTRANT -L. -lSDL2 -lSDL2_ttf -DWINDOWS

clean:
	rm *.o
