Copyright 2020-2021 Autumn Dawn


Disclaimer: on this version, the code is much more trash than on that commandline version. Also, I have no idea how to manage licences, I hope I did well enough.

Description
-----------
You need to collect apples (green) to win. Avoid traps (red).


Use arrows or WASD to move around.
Use 'g' to adjust ability.
Use 'f' to activate ability, then choose direction.
Use 'b' to drop a bomb, which explodes after time equal to your ability modifier (adjusted under 'g').
Use 'v' to show "framerate". It doesn't work as it should, though.


Earthponies have passive ability that lets them disarm traps, making them harmless to them;
they can also jump two fields forward and destroy obstacles (more easily).

Unicorns and pegasi can both use their special abilities to jump several fields forward.
Unicorns teleport in just one turn, but have thrice as long cooldown, while pegasi take 1 additional turn per every 2 fields flown, and have constant cooldown per given difficulty.
Also, unicorns can use their ability through obstacles, while pegasi (and earthponies) cannot, but they can also collect apples on the way. Also, try to avoid targeting traps.


You also need to avoid the wolves (brown) and jumpers (purple).

Configuration
-------------
The first config file value is "framerate". Basically the higher it is, the higher the framerate, but higher CPU usage (thanks to my terrible optimization).
The second value is scaling, '1' is the default. 0.8 is 80% the normal size, 1.2 is 120%, etc.


Compiling
---------

Dependencies:

- SDL2

- SDL2_ttf

### Linux and Haiku

Use `make objs` then `make pca` to compile GCC version, use `make objs-clang` then `make pca-clang` to compile with Clang instead. Or you can mix them up, for whatever reason.


### Windows

You're on your own.

Debug version
-------------
Debug version contains the following commands:

- `.` - add health.
- `,` - decrease health.
- `'` - increase difficulty.
- `;` - decrease difficulty.
- `]` - increase level.
- `[` - decrease level.
